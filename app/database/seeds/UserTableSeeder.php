<?php
 
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->delete();
 
        User::create(array(
            'username' => 'user1',
            'password' => Hash::make('123456')
        ));
 
        User::create(array(
            'username' => 'user2',
            'password' => Hash::make('123456')
        ));
    }
 
}
