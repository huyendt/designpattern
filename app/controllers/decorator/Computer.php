<?php
	namespace decorator;
	class Computer 
	{

		public $description = '';

		public function __construct($computer)
		{
			$this->description = $computer;
		}

	}
