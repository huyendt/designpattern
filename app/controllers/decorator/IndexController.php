<?php
	namespace decorator;
	require_once 'Computer.php';
	require_once 'ComponentDecorator.php';

	class IndexController extends \BaseController 
	{
		public function index() {
			$computer = new Computer('computer');
			$decorator1 = new Disk($computer);
			$decorator2 = new CD($computer);

			echo $decorator1->description();
			echo '<br>';
			echo $decorator2->description();
		}
	}
