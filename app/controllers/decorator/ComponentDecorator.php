<?php
	namespace decorator;
	include_once 'Computer.php';
	class ComponentDecorator
	{
		private $_computer;

		public function __construct($_computer)
		{
			$this->_computer = $_computer;
		}

		public function description()
		{
			return $this->_computer->description . ' ' . $this->_computer->description;
		}
	}

	class Disk extends ComponentDecorator
	{
		private $_computer;

		public function __construct($_computer)
		{
			$this->_computer = $_computer;
		}

		public function description()
		{
			return $this->_computer->description . ' + ' . 'add a disk';
		}
	}

	class CD extends ComponentDecorator
	{
		private $_computer;

		public function __construct($_computer)
		{
			$this->_computer = $_computer;
		}

		public function description()
		{
			return $this->_computer->description . ' + ' . 'add a CD';
		}
	}

