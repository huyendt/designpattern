<?php
namespace factory;
require_once 'Factory.php';
require_once 'Thing.php';
class IndexController extends \BaseController
{
	public function index() 
	{
		$thing1 = FactoryThing::create('Paypal');	
		$thing2 = FactoryThing::create('Credit');	
		echo $thing1->process();
		echo "<br>";
		echo $thing2->process();
	}
}