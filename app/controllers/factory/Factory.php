<?php
namespace factory;
include_once 'Thing.php';
class FactoryThing
{
	public static function create($input)
	{
		$model = new $input();
		return $model;
	}
}
