<?php
namespace observer;
include_once 'Boss.php';
include_once 'Staff.php';
class ObserverController extends \BaseController {

	public function index() {
		$boss = new Boss;
		 
		$staff1 = new Staff("Huyen");
		$staff2 = new Staff("Thao");
		$staff3 = new Staff("Lan");
		 
		$boss->attach($staff1);
		$boss->attach($staff2);
		$boss->attach($staff3);
		 
		 
		$boss->load();
		 
		echo "<br />----------Are you ready?------------<br /><br />";
		 
		$boss->happy("beer");
		 
		echo "<br />-----Good! Our give is finished.-----<br /><br />";

		$boss->angry("scolded");
		 
		echo "<br />-----Good! Our give is finished.-----<br /><br />";
		 
		$boss->flush();
	}
}