<?php 
namespace observer;
include_once 'Staff.php';
	class Boss
	{
		//Mang nay chua toan bo nhan vien co trong he thong
	 	protected $bots = array();
 
	 	//qua nhan dc
	 	public $give;
 	
	 	//Dang ky la 1 nhan vien trong he thong
	 	public function attach(Staff $bot)
	 	{
	 		$this->bots[] = $bot;

	 	} 
	 	//Huy dang ki 1 nhan vien co trong he thong
	 	public function dettach(Staff $bot)
	 	{
	 		foreach($this->bots as $key => $system_bot)
	 		{
	 			unset($this->bots[$key]);
	 		}
	 	} 
	 	//Load tat ca nhan vien vao he thong
	 	public function load()
	 	{
	 		foreach($this->bots as $bot)
	 		{
	 			$bot->login();
	 		}
	 	} 
	 	//Giai tan nhan vien
	 	public function flush()
	 	{
	 		foreach($this->bots as $bot)
	 		{
	 			$bot->logout();
	 		}
	 	} 
	 	//Vui thì chi dinh cty an che
	 	public function happy($_give)
	 	{
	 		$this->give = $_give;
	 		foreach($this->bots as $bot)
	 		{
	 			$bot->drink($this);
	 		}
	 	}
	 	//Gian thì ca cty an chui
	 	public function angry($_give)
	 	{
	 		$this->give = $_give;
	 		foreach($this->bots as $bot)
	 		{
	 			$bot->scolded($this);
	 		}
	 	} 
	}
