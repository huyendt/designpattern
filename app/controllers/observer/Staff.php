<?php
namespace observer;
include_once 'Boss.php';

class Staff 
{
	//Ten Nhan vien
	protected $name;
 
	public function __construct($bot_name){
		$this->name = $bot_name;
	}
 
	//Tap hop
	public function login(){
		echo "$this->name was logged in.<br />";
	}
 
	//Giai tan
	public function logout(){
		echo "$this->name was logged out.<br />";
	}
 
	//Uong bia
	public function drink(Boss $boss){
		echo "$this->name is drunk $boss->give.<br />";
	}

	//Nghe mang 
	public function scolded(Boss $boss){
		echo "$this->name is $boss->give.<br />";
	}
}